package main

import (
	"fmt"
	"net/http"

	"gitlab.com/qatoolist/go-rest/internal/comment"
	"gitlab.com/qatoolist/go-rest/internal/database"
	transportHTTP "gitlab.com/qatoolist/go-rest/internal/transport/http"
)

// App - the struct wihch contains the things like pointers
type App struct {
}

// Run - Sets up our application
func (app *App) Run() error {
	fmt.Println("Setting up our app")

	var err error
	db, err := database.NewDatabase()
	if err != nil {
		return err
	}

	err = database.MigrateDB(db)
	if err != nil {
		return err
	}

	commentService := comment.NewService(db)

	handler := transportHTTP.NewHandler(commentService)
	handler.SetupRoutes()

	if err := http.ListenAndServe(":8080", handler.Router); err != nil {
		fmt.Println("Failed to set up server")
	}

	return nil
}

func main() {
	fmt.Println("Go Rest API Project")
	app := App{}
	if err := app.Run(); err != nil {
		fmt.Println("Error starting up our REST API")
		fmt.Println(err)
	}
}
