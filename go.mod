module gitlab.com/qatoolist/go-rest

go 1.15

require (
	github.com/go-resty/resty/v2 v2.7.0
	github.com/gorilla/mux v1.8.0
	github.com/jinzhu/gorm v1.9.16
	github.com/stretchr/testify v1.7.0
)
